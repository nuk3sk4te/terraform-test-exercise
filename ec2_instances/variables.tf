variable "ami_id" {
  type        = string
  description = "Name of the AMI to use"
}

variable "key_name" {
  type        = string
  description = "Name of the KEY pair to use on EC2 Instances"
}

variable "subnet_ids" {
  type        = list(string)
  description = "List of subnet IDs for the instances"
}

variable "security_group_name" {
  type        = string
  description = "Name of the security group"
}

variable "ebs_volume_size" {
  type        = string
  description = "EBS Volume size"
  default     = "20G"
}
