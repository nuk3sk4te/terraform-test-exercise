resource "aws_security_group" "ipfs_sg" {
  name        = var.security_group_name
  description = "IPFS Security Group"

  ingress {
    from_port   = 4001
    to_port     = 4001
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


resource "aws_instance" "ec2_instances" {
  count         = length(var.subnet_ids)
  ami           = var.ami_id
  instance_type = "t2.medium"
  key_name      = var.key_name
  availability_zone = element(["eu-west-1a", "eu-west-1b", "eu-west-1c"], count.index)
  subnet_id     = element(var.subnet_ids, count.index)
  vpc_security_group_ids = [aws_security_group.ipfs_sg.id]
}

resource "aws_ebs_volume" "ebs_volumes" {
  count             = length(var.subnet_ids)
  availability_zone = element(["eu-west-1a", "eu-west-1b", "eu-west-1c"], count.index)
  size              = var.ebs_volume_size
}

resource "aws_volume_attachment" "ebs_attachments" {
  count       = length(var.subnet_ids)
  device_name = "/dev/xvda"
  instance_id = aws_instance.ec2_instances[count.index].id
  volume_id   = aws_ebs_volume.ebs_volumes[count.index].id
}

output "instance_ids" {
  value = aws_instance.ec2_instances[*].id
}

output "volume_ids" {
  value = aws_ebs_volume.ebs_volumes[*].id
}