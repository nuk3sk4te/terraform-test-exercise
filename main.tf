module "ec2_instances" {
  source              = "./ec2_instances"
  ami_id              = var.ami_id
  key_name            = var.key_name
  subnet_ids          = var.subnet_ids
  security_group_name = "ipfs-security-group"
  ebs_volume_size     = var.ebs_volume_size
}

output "instance_ids" {
  value = module.ec2_instances.instance_ids
}

output "volume_ids" {
  value = module.ec2_instances.volume_ids
}