variable "aws_profile" {
  type        = string
  description = "Name of the AWS CLI profile to use for authentication"
}

variable "ami_id" {
  type        = string
  description = "Name of the AMI to use"
  default     = "ami-08f32efd140b7d89f"
}

variable "key_name" {
  type        = string
  description = "Name of the KEY pair to use on EC2 Instances"
}

variable "security_group_name" {
  type        = string
  description = "Name of the security group"
}

variable "subnet_ids" {
  type        = list(string)
  description = "List of subnet IDs where EC2 instances will be deployed"
}

variable "ebs_volume_size" {
  type        = string
  description = "EBS Volume size"
  default     = "20G"
}