# variables.tfvars

aws_profile         = "default"
ami_id              = "ami-08f32efd140b7d89f"
key_name            = "ipfs-keypair-name"
subnet_ids          = ["private-subnetid-1", "private-subnetid-2", "private-subnetid-3"]
security_group_name = "sg-ipfs-arn"
ebs_volume_size     = "100"
