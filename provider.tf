provider "aws" {
  region  = "eu-west-1"
  profile = var.aws_profile
  assume_role {
    role_arn = "arn:aws:iam::blabla:role/adminrole"
  }
}



terraform {
  required_version = ">=0.14.0"
  backend "s3" {
    bucket         = "dvop-s3bkt-dev-euw1-terraform"
    key            = "dvop-s3bkt-dev-euw1-terraform.tfstate"
    dynamodb_table = "state-locking"
  }
}
